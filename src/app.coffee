import { Elm } from "./Main.elm"

node = document.getElementById "app-container"
flags = {}

Elm.Main.init { node, flags }
