# NOTE: GNU Make is supposed to be run in an environment controlled by Nix.
# If it's not automatically loaded by direnv and lorri, then call:
#
#     make shell
#
# first.

export PATH := $(nodeDependencies)/bin:$(PATH)
export ELM_HOME := $(PWD)/.elm

result: clean node-dependencies.nix elm-packages.nix $(shell find src -type f)
	nix-build


.PHONY: develop
develop: node_modules
	parcel src/index.html

.PHONY: serve
serve: result
	serve result

.PHONY: clean
clean:
	git clean --force -d -X

node-dependencies.nix: package-lock.json global-npm-packages.json
	rm -rf node_modules
	node2nix \
		--development \
		--lock package-lock.json \
		--supplement-input global-npm-packages.json \
		--composition $@

package-lock.json: package.json
	npm install --package-lock-only

elm-packages.nix: elm.json elm-registry.dat
	elm2nix convert > $@

elm-registry.dat:
	# Workaround for https://github.com/cachix/elm2nix/issues/43
	elm2nix snapshot > registry.dat
	mv registry.dat $@

# Call only if not using direnv and Lorri. It will be slow. Use Lorri!
.PHONY: shell
shell:
	nix-shell

# Those targets are run by nix during build

dist: node_modules .elm
	parcel build src/index.html

node_modules: node-dependencies.nix
	cp -r $(nodeDependencies)/lib/node_modules ./node_modules
	chmod --recursive u+w ./node_modules

.elm:
	$(fetchElmDeps)

.PHONY: install
install: dist
	mkdir -p $(out)
	cp -r dist/* $(out)
