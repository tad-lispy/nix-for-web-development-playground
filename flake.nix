{
  description = "A playground to figure out how to use Nix for web development (Node.js, Elm, PostgREST)";

  inputs = {
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
    flake-utils = {
      url = "github:numtide/flake-utils";
    };
    migra = {
      url = "gitlab:tad-lispy/migra/nix";
    };
  };

  outputs = { self, nixpkgs , flake-compat , flake-utils, migra }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = import nixpkgs {
          inherit system;
        };
        nodeDependencies = (
          pkgs.callPackage ./node-dependencies.nix {}
        ).shell.nodeDependencies;
        fetchElmDeps = pkgs.elmPackages.fetchElmDeps {
          elmPackages = import ./elm-packages.nix;
          registryDat = ./elm-registry.dat;
          elmVersion = (builtins.fromJSON (
            builtins.readFile ./elm.json
          )).elm-version;
        };
      in rec {
        packages.nix-for-web-development-playground = pkgs.stdenv.mkDerivation {
          name = "nix-for-web-development-playground";
          src = self;
          buildInputs = [
            pkgs.git
            pkgs.nodejs
            pkgs.utillinux
            pkgs.elmPackages.elm
            pkgs.elm2nix
            pkgs.nodePackages.node2nix
            pkgs.httpie
            nodeDependencies
            pkgs.postgresql
            migra.defaultPackage.${system}
          ];
          buildPhase = "make dist";

          inherit nodeDependencies fetchElmDeps;
        };
        defaultPackage = packages.nix-for-web-development-playground;
      }
    );
}
